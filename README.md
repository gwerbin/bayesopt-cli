# Bayesopt CLI

## Installation

This currently depends on an [unofficial fork](https://github.com/darenr/scikit-optimize) of Scikit-optimize, which seems to be
abandoned by its original developer.

```bash
pip install 'git+https://github.com/darenr/scikit-optimize@master#egg=scikit-optimize'
pip install 'git+https://gitlab.com/gwerbin/bayesopt-cli@master#egg=bayesopt-cli'
```

## Usage

See `./example` for more-detailed usage demonstrations.

See `bayesopt --help`:

```
Usage: bayesopt [OPTIONS] SPACE_FILENAME OUTPUT_FILENAME COMMAND [ARGS]...

  Optimize the output of a command.

Options:
  --version                       Show the version and exit.
  -s, --strategy [gp|forest|gbrt|dummy]
                                  Optimization strategy.
  -g, --greater-is-better         Enable to maximize, not minimize, objective.
  -r, --random-seed INTEGER       Random seed for optimizer.
  -n, --n-calls INTEGER           Number of sequential optimization runs.
  -k, --space-key TEXT            Top-level dict key in the YAML input, for
                                  use as parameters; if the YAML is an array,
                                  this is ignored.
  -j, --jmespath TEXT             JMESPath to extract score from command
                                  output, if command outputs JSON.
  -v, --verbose                   Enable verbose optimizer output
  -p, --print-summary             Print a YAML summary of results on
                                  completion.
  --help                          Show this message and exit.

  The command must output either a real number or JSON from which a real
  number can be extracted.

  Use -- to prevent arguments from being parsed by this application. For
  example:

      bayesopt -- ./train-eval.py -v

  Without --, the -v option would be captured by bayesopt, instead of being
  passed to the script.

  Optimization results are saved in Pickle format, using the Scikit-optimize
  dump routine. Any of these filename suffixes will apply the corresponding
  compression algorithm: ".z", ".gz", ".bz2", ".xz", ".lzma".

  The YAML space specification must deserialize to a dictionary/mapping or a
  list/array. See example/space.yaml for an example in mapping format. If a
  mapping is used, specify the --space-key option to tell which key to use
  from the mapping. If this option is omitted, use the first one by
  position.
```
