from setuptools import setup

setup(
    name='bayesopt-cli',
    version='0.2.0',

    install_requires=['click', 'jmespath', 'scikit-optimize>=0.6', 'pyaml'],
    python_requires='>=3.6',

    py_modules=['bayesopt'],
    entry_points={
        'console_scripts': [
            'bayesopt = bayesopt:main'
        ]
    }
)

