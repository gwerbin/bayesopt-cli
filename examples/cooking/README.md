This example uses Fasttext, and is based on the [Fasttext tutorial](https://fasttext.cc/docs/en/supervised-tutorial.html).

To run, simply execute
```bash
make
```
