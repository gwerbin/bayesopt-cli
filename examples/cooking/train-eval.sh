#!/bin/bash

set -eu -o pipefail

echo $@ >> log.txt

train_file=$1; shift
test_file=$1; shift
model_file_base=$1; shift

run_id="$( < /dev/urandom od -vAn -N2 -tu2 | sed 's/ //g' )"

model_file=${model_file_base}_${run_id}

fasttext='/home/p712003/.pyenv/versions/miniconda3-latest/envs/tts-classification-3/bin/fasttext'

$fasttext supervised -verbose 0 \
	-input "$train_file" \
	-output "$model_file" \
	-loss hs \
	-bucket 200000 \
	${1+-epoch $1} \
	${2+-lr $2} \
	${3+-wordNgrams $3} \
	${4+-dim $4}

$fasttext test "${model_file}.bin" "$test_file" | awk '/R@1/ { print $2 }'

