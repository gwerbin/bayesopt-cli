#!/bin/bash

set -eu -o pipefail

score=$( bc <<< "$1 + $2" )
score=$( printf "%0.0f" $score )
guid=$( < /dev/urandom head -c 8 | base64 | tr '"' '~' )
echo "{\"guid\": \"$guid\", \"result\": { \"score\": $score } }"

