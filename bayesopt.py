import json
import subprocess
import warnings
from pkg_resources import get_distribution

import click
import jmespath
import skopt
import pyaml


__version__ = get_distribution('bayesopt-cli').version


class SubprocesFailed(click.ClickException):
    def __init__(self, cp):
        self.cp = cp
        message = f'Command exited with status {cp.returncode}. Stderr:\n\n{cp.stderr}\n'
        super().__init__(message)

    def format_message(self):
        return self.message


def raise_for_status(cp):
    if cp.returncode != 0:
        raise SubprocesFailed(cp)


def parse_output(cp, jpath=None):
    if jpath is None:
        score = float(cp.stdout.strip())
    else:
        try:
            data = json.loads(cp.stdout)
        except json.JSONDecodeError as exc:
            raise click.ClickException(f'Output is not valid JSON; parse error: {exc!s}\nOutput:\n{cp.stdout!s}')

        score = jpath.search(data)
        try:
            score = float(score)
        except TypeError:
            raise click.ClickException(f'JMESPath query did not return a real number.')

    return score


def make_objective(cmd, jpath=None, greater_is_better=False):
    sign = -1 if greater_is_better else 1

    def objective(params):
        cp = subprocess.run((*cmd, *map(str, params)), stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding='utf-8')
        raise_for_status(cp)
        score = parse_output(cp, jpath)
        return sign * score

    return objective


def summarize_result(result, greater_is_better=False):
    sign = -1 if greater_is_better else 1

    def clean_x(vals):
        return [sign * float(val) for val in vals]

    return {
        'argbest': clean_x(result.x),
        'best': sign * float(result.fun),
        'iterations': [{'x': clean_x(x), 'y': sign * float(y)} for x, y in zip(result.x_iters, result.func_vals)],
        #'final_random_state': result.rng,
    }


OPTIMIZERS = {
    'gp': skopt.gp_minimize,
    'forest': skopt.forest_minimize,
    'gbrt': skopt.gbrt_minimize,
    'dummy': skopt.dummy_minimize
}


# TODO: x0, y0 ?
# TODO: skopt.dump kwargs passed to joblib.dump

@click.command(
    'bayesopt',

    help='''Optimize the output of a command.''',

    epilog=
'''The command must output either a real number or JSON from which a real number can be extracted.

Use -- to prevent arguments from being parsed by this application. For example:

    bayesopt -- ./train-eval.py -v

Without --, the -v option would be captured by bayesopt, instead of being passed to the script.


Optimization results are saved in Pickle format, using the Scikit-optimize dump routine. Any of these
filename suffixes will apply the corresponding compression algorithm: ".z", ".gz", ".bz2", ".xz", ".lzma".

The YAML space specification must deserialize to a dictionary/mapping or a list/array. See example/space.yaml for an example in mapping format.
If a mapping is used, specify the --space-key option to tell which key to use from the mapping. If this option is omitted, use the first one by position.'''
)
@click.version_option(__version__, prog_name='Bayesopt CLI')
@click.option('--strategy', '-s', type=click.Choice(list(OPTIMIZERS), case_sensitive=False), default='gp', help='Optimization strategy.')
@click.option('--greater-is-better', '-g', is_flag=True, default=False, help='Enable to maximize, not minimize, objective.')
@click.option('--random-seed', '-r', type=click.INT, default=None, help='Random seed for optimizer.')
@click.option('--n-calls', '-n', type=click.INT, default=10, help='Number of sequential optimization runs.')
@click.option('--space-key', '-k', default=None, help='Top-level dict key in the YAML input, for use as parameters; if the YAML is an array, this is ignored.')
@click.option('--jmespath', '-j', 'jpath', default=None, help='JMESPath to extract score from command output, if command outputs JSON.')
@click.option('--verbose', '-v', is_flag=True, default=False, help='Enable verbose optimizer output')
@click.option('--print-summary', '-p', is_flag=True, default=False, help='Print a YAML summary of results on completion.')
@click.argument('space_filename', type=click.Path(exists=True, dir_okay=False))
@click.argument('output_filename', type=click.Path(exists=False, dir_okay=False))
@click.argument('command', required=True)
@click.argument('args', nargs=-1)
def main(strategy, greater_is_better, random_seed, n_calls, space_key, jpath, verbose, print_summary, space_filename, output_filename, command, args):
    cmd = (command, *args)

    optimizer = OPTIMIZERS[strategy]

    if jpath is not None:
        jpath = jmespath.compile(jpath)

    objective = make_objective(cmd, jpath=jpath, greater_is_better=greater_is_better)

    kwargs = {
        'verbose': verbose,
        'n_calls': n_calls,
        'random_state': random_seed
    }

    with warnings.catch_warnings():
        warnings.filterwarnings('ignore', module='skopt|yaml')
        dimensions = skopt.Space.from_yaml(space_filename, namespace=space_key).dimensions

        optim_result = optimizer(objective, dimensions, **kwargs)

    skopt.dump(optim_result, output_filename, store_objective=False)

    if print_summary:
        summary = summarize_result(optim_result, greater_is_better=greater_is_better)
        click.echo(pyaml.dumps(summary, safe=True, sort_dicts=False))


if __name__ == '__main__':
    main()

